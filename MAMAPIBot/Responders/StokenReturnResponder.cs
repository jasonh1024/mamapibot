﻿using System;
using System.Configuration;
using System.Text;
using API.ChatApi;
using MargieBot;
using Newtonsoft.Json.Linq;

namespace MAMAPIBot.Responders
{
    public class StokenReturnResponder : IResponder
    {
        private readonly IChatApi chatApi;

        public StokenReturnResponder(IChatApi chatApi)
        {
            this.chatApi = chatApi;
        }

        public bool CanRespond(ResponseContext context)
        {
            return context.Message.MentionsBot
                && !context.BotHasResponded
                   && context.Message.Text.Contains("\"stoken\":");
        }

        public BotMessage GetResponse(ResponseContext context)
        {
            string fullResponse = context.Message.Text;
            string response = null;
            string stoken = null;
            string service = null;

            //to get index for stoken
            int startIndex = fullResponse.IndexOf(":") + 1;
            int endIndex = fullResponse.IndexOf(",") - 1;

            //to get index for service
            int startIndexService = fullResponse.IndexOf(":", startIndex) + 1;
            service = fullResponse.Substring(startIndexService + 1, 1);

            AppleClient appleAPI = new AppleClient();
            var builder = new StringBuilder();

            if ((endIndex - startIndex - 1 != 0) && (service == "1"))
            {
                stoken = fullResponse.Substring(startIndex + 1, endIndex - startIndex - 1);
                try
                {
                    response = appleAPI.CheckStokenAsync(stoken).Result;
                    response = JToken.Parse(response).ToString();
                    builder.Append(response);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            else if (service == "2")
            {
                stoken = fullResponse.Substring(startIndex + 1, endIndex - startIndex - 1);
                try
                {
                    response = appleAPI.GetVPPAssetsSrvAsync(stoken).Result;
                    response = JToken.Parse(response).ToString();
                    builder.Append(response);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            else 
            {
                response = "format incorrect";
                builder.Append(response);
            }
            
            builder.Append(context.Message.User.FormattedUserID);

            chatApi.PostMessage(ConfigurationManager.AppSettings["SlackBotApiToken"], context.Message.User.ID, "Jason, stoken is" + stoken + service);

            return new BotMessage { Text = builder.ToString() };
        }
    }
}
