﻿using System;
using System.Configuration;
using System.Text;
using API.ChatApi;
using MargieBot;
using Newtonsoft.Json.Linq;

namespace MAMAPIBot.Responders
{
    public class StokenCheckResponder : IResponder
    {
        private readonly IChatApi chatApi;

        public StokenCheckResponder(IChatApi chatApi)
        {
            this.chatApi = chatApi;
        }

        public bool CanRespond(ResponseContext context)
        {
            return context.Message.MentionsBot
                && !context.BotHasResponded
                   && context.Message.Text.ToLower().Contains("check stoken");
        }

        public BotMessage GetResponse(ResponseContext context)
        {
            var builder = new StringBuilder();
            builder.Append("Please use the following format to call service.");
            builder.Append(context.Message.User.FormattedUserID + Environment.NewLine).AppendLine();
            builder.AppendLine("Service#, Service Name");

            //add different types of services
            string[] serviceN = { "Check Stoken", "Check Assets(Licenses)", "Revoke Licenses(in building)"};
            int counter = 0;
            foreach (string value in serviceN)
            {
                builder.AppendFormat("{0} {1}.\n",
                    ++counter,
                    value);
            }
            builder.AppendLine().AppendLine("------Example: to check stoken------");
            //json example
            string body = "{stoken: \"your stoken here\",service: 1}";
            body = JToken.Parse(body).ToString();
            builder.Append(body).AppendLine().AppendLine("------------------------------------");

            //serach app example
            builder.Append("If you wanna search app. Please type in \"Search:{your_app_name/adam_id}");

            chatApi.PostMessage(ConfigurationManager.AppSettings["SlackBotApiToken"], context.Message.User.ID, "Jason, somebody is trying to check stoken");

            return new BotMessage { Text = builder.ToString() };
        }
    }
}
