﻿using System;
using System.Configuration;
using System.Text;
using API.ChatApi;
using MargieBot;
using Newtonsoft.Json.Linq;

namespace MAMAPIBot.Responders
{
    public class AppSearchResponder : IResponder
    {
        private readonly IChatApi chatApi;

        public AppSearchResponder(IChatApi chatApi)
        {
            this.chatApi = chatApi;
        }

        public bool CanRespond(ResponseContext context)
        {
            return context.Message.MentionsBot
                && !context.BotHasResponded
                   && context.Message.Text.ToLower().Contains("search:{");
        }

        public BotMessage GetResponse(ResponseContext context)
        {
            string fullResponse = context.Message.Text;
            string response = null;
            string appName = null;

            int startIndex = fullResponse.IndexOf("{") + 1;
            int endIndex = fullResponse.IndexOf("}");

            //to get index for service
            appName = fullResponse.Substring(startIndex, endIndex - startIndex);

            AppleClient appleAPI = new AppleClient();
            var builder = new StringBuilder();
            bool isNumeric = int.TryParse(appName, out int n);

            if (isNumeric)
            {
                try
                {
                    int adamID = int.Parse(appName);
                    response = appleAPI.SearchAdamIDAsync(adamID).Result;
                    response = JToken.Parse(response).ToString();
                    builder.Append(response);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            else
            {
                try
                {
                    response = appleAPI.SearchAppByName(appName);
                    response = JToken.Parse(response).ToString();
                    //builder.AppendLine(response);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

            //builder.Append(context.Message.User.FormattedUserID);
            response = response + context.Message.User.FormattedUserID;
            chatApi.PostMessage(ConfigurationManager.AppSettings["SlackBotApiToken"], context.Message.User.ID, "Jason, i am searching app" + appName);
            return new BotMessage { Text = response }; //builder.ToString()
        }
    }
}
