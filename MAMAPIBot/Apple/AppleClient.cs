﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Web;
using System.IO;

namespace MAMAPIBot
{
    public class AppleClient
    {
        //parameters
        private string jsonResponse;

        public string JsonResponses
        { get
            {
                return this.jsonResponse;
            }
            set
            {
                this.jsonResponse = value;
            }
        }

        public async Task<string> CheckStokenAsync(string stokenValue)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var body = new Body { sToken = stokenValue };

                // Serialize our concrete class into a JSON String
                var stringPayload = JsonConvert.SerializeObject(body);

                // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                try
                {
                    //response = httpClient.PostAsync("https://vpp.itunes.apple.com/WebObjects/MZFinance.woa/wa/VPPClientConfigSrv", httpContent).Result;
                    response = await httpClient.PostAsync("https://vpp.itunes.apple.com/WebObjects/MZFinance.woa/wa/VPPClientConfigSrv", httpContent);
                }
                catch (Exception ex)
                {
                    if (response == null)
                    {
                        Console.WriteLine("nothing");
                    }
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    response.ReasonPhrase = string.Format("request failed: {0}", ex);

                }

                if (response.IsSuccessStatusCode)
                {
                    //jsonResponse = response.Content.ReadAsStringAsync().Result;
                    jsonResponse = await response.Content.ReadAsStringAsync();
                }
                return jsonResponse;
            }
        }

        public async Task<string> GetVPPAssetsSrvAsync(string stokenValue)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var body = new Body { sToken = stokenValue , includeLicenseCounts = "true" };

                // Serialize our concrete class into a JSON String
                var stringPayload = JsonConvert.SerializeObject(body);

                // Wrap our JSON inside a StringContent which then can be used by the HttpClient class
                var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;
                try
                {
                    response = await httpClient.PostAsync("https://vpp.itunes.apple.com/WebObjects/MZFinance.woa/wa/getVPPAssetsSrv", httpContent);
                }
                catch (Exception ex)
                {
                    if (response == null)
                    {
                        Console.WriteLine("nothing");
                    }
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    response.ReasonPhrase = string.Format("request failed: {0}", ex);

                }

                if (response.IsSuccessStatusCode)
                {
                    //jsonResponse = response.Content.ReadAsStringAsync().Result;
                    jsonResponse = await response.Content.ReadAsStringAsync();
                }
                return jsonResponse;
            }
        }

        public string SearchAppByName(string appName)
        {
            //using (HttpClient httpClient = new HttpClient())
            //{
            //    //url encode
            //    string para = HttpUtility.UrlEncode(appName);
            //    HttpResponseMessage response = null;
            //    try
            //    {
            //        response = await httpClient.GetAsync("https://itunes.apple.com/search?term=" + para + "&country=us");
            //    }//need to check why this is just returning header
            //    catch (Exception ex)
            //    {
            //        if (response == null)
            //        {
            //            Console.WriteLine("nothing");
            //        }
            //        response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            //        response.ReasonPhrase = string.Format("request failed: {0}", ex);

            //    }

            //    if (response.IsSuccessStatusCode)
            //    {
            //        //jsonResponse = response.Content.ReadAsStringAsync().Result;
            //        string filepath = @"c:\temp\1.txt";
            //        using (
            //            Stream contentStream = await response.Content.ReadAsStreamAsync(),
            //            stream = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.None, 1024, true))
            //        {
            //            await contentStream.CopyToAsync(stream);
            //        }

            //        if (File.Exists(filepath))
            //        {
            //            jsonResponse = File.ReadAllText(filepath);
            //        }
            //    }
            //}
            string para = HttpUtility.UrlEncode(appName);
            jsonResponse = "https://itunes.apple.com/search?term=" + para + "&country=us";
            return jsonResponse;
        }

        public async Task<string> SearchAdamIDAsync(int adamID)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                HttpResponseMessage response = null;
                try
                {
                    response = await httpClient.GetAsync("https://uclient-api.itunes.apple.com/WebObjects/MZStorePlatform.woa/wa/lookup?version=2&id=" + adamID 
                        + "&p=mdm-lockup&caller=MDM&platform=enterprisestore&cc=us&l=en");
                }
                catch (Exception ex)
                {
                    if (response == null)
                    {
                        Console.WriteLine("nothing");
                    }
                    response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                    response.ReasonPhrase = string.Format("request failed: {0}", ex);

                }

                if (response.IsSuccessStatusCode)
                {
                    //jsonResponse = response.Content.ReadAsStringAsync().Result;
                    jsonResponse = await response.Content.ReadAsStringAsync();
                }
                return jsonResponse;
            }
        }

        public class Body
        {
            [JsonProperty("sToken")]
            public string sToken { get; set; }

            [JsonProperty("includeLicenseCounts")]
            public string includeLicenseCounts { get; set; }



        }
    }
}
